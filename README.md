# windify

Easily create and handle floating windows.

For the full documentation, installation instructions... check [windify documentation page](https://squeak.eauchat.org/libs/windify/).
