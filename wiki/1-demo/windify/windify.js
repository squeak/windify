var windify = require("../../../core");
var uify = require("uify");

window.demoStart = function (utilities) {

  utilities.section({
    title: "windify",
    description: "Create a floating window with any type of content.",
    demo: function ($container) {

      //
      //                              START

      $container.css({
        height: 400,
        position: "relative",
        overflow: "hidden",
      });
      newButton($container);

      //
      //                              BUTTON CREATOR

      function newButton ($container) {
        uify.button({
          $container: $container,
          icomoon: "rocket",
          title: "click me",
          css: { fontSize: "3em", },
          inlineTitle: "bottom",
          click: function () { newWindow(); },
        });
      };

      //
      //                              WINDOW CREATOR

      function newWindow () {
        windify({
          $container: $container,
          title: "My window",
          bodyColor: $$.random.color(0.7),
          titleBarColor: $$.random.color(0.7),
          contentMaker: function ($windowBody) {
            $windowBody.div({ text: "you can close me whith 'shift + w'"});
            newButton($windowBody);
          },
        });
      };

      //                              ¬
      //

    },

  });

};
