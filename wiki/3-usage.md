# Usage

```javascript
const windify = require("windify");
windify({
  $container: $("#element-that-will-contain-window"),
  title: "Some window",
  contentMaker: function ($windowBody) {
    $windowBody.text("Some content.");
  },
})
```
