
module.exports = {

  title: "API",
  type: "api",

  moduleName: "windify",

  documentation: {
    methods: [
      //
      //                              WINDIFY

      {
        name: "windify",
        path: "core/index",
        methodsAutofill: {
          type: "all",
          excluded: ["windify.windify"],
        },
      },

      //
      //                              WINDIFIED

      {
        name: "windified",
        matcher: false,
        display: {
          comment: "Methods available in windified object (the one returned when running windify function).",
        },
        path: "core/methods",
        methodsAutofill: "all",
      },

      //                              ¬
      //
    ],
  },

};
