
Create floating windows in your page with a code a simple as this:

```js
windify({
  title: "My window",
  contentMaker: function ($windowBody) {
    // create content here
  }
});
```
