var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/dom");
var keyboardify = require("keyboardify");
var $ = require("yquerj");
var uify = require("uify");
var methods = require("./methods");
var titlebar = require("./titlebar");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: create a windify window
  ARGUMENTS: (options <{
    ?$container: <yquerjObject>@default=$("body") « dom element in which to create the window »,
    ?title: <string> « title of the window »,
    ?subtitle: <string> « subtitle of the window »,
    ?icon: <string> « icon of the window »,
    ?titleBarColor: <string> « color of the title bar »,
    ?bodyColor: <string> « color of the window body »,
    ?buttons: <uify.button·options[]> « list of buttons in the window titlebar »,
    ?buttonsContext <any> « context passed to titlebar buttons (will be the this of click callbacks functions for those buttons) »,
    ?contentMaker: <function($body){@this=windified}> « make the content of the window »,
    ?close: <function> « custom function to execute when the cross button is clicked »,
    ?draggable: <boolean>@default=true « pass false here if you want the window not to be draggable »,
    ?css: <object> « custom css to apply to the window »,
    ?windifyOptionsMaker: <function(<windify·options>):<windify·options>> « allow you to add any options that will be recalculated every time the window is refreshed »,
  }>)
  $window: <yquerjObject> « the window dom element »,
  RETURN: windified <{
    $body: <yquerjObject> « the content part of the window »,
    windowId: <string> « a randomly set uuid that identified uniquely this windify window »,
    isWindified: <true>,
    options: <windifyOptions> « passed options at window creation »,
    global: <{
      instances: <{ [randomUUID]: <windified> }> « list of the currently opened windified windows »,
      selectionHistory: [] « list of windified windows in the order they were last selected (list is redundant, every time a window is selected, its windified is added at the beginning of the list) »,
      topWindowZIndex: <number> « z-index of the windify windows that is currently most in the foreground »,
    }>,
    ... see full list of windified methods
  }>;

*/
var windify = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GLOBAL INFORMATIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // GLOBAL INFORMATIONS
  global: {
    instances: {},
    selectionHistory: [],
    topWindowZIndex: 1,
    events: {
      create: [],
      destroy: [],
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAIN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    see windify
  */
  main: function (options) {

    //
    //                              HANDLE OPTIONS

    options = windify.calculateOptions(options);

    //
    //                              WINDIFIED OBJECT

    var windified = {
      isWindified: true,
      $container: options.$container,
      options: options,
      windowId: $$.uuid(),
      global: windify.global,
      getActiveWindow: windify.getActiveWindow,
      calculateOptions: windify.calculateOptions,
    };

    //
    //                              ATTACH METHODS

    _.each(methods, function (methodFunc, methodName) {
      windified[methodName] = _.bind(methodFunc, windified);
    });

    //
    //                              CREATE DOM ELEMENTS

    windified.$window = options.$container.div({ class: "windify", }).mousedown(function () { windified.active(); });
    if (options.class) windified.$window.addClass(options.class);
    if (options.css) windified.$window.css(options.css);
    windified.titlebar = titlebar(windified);
    if (options.draggable) {
      $$.dom.makeDraggable({ $target: windified.$window, $handle: windified.titlebar.$el, });
      $$.dom.makeDraggable({ $target: windified.$window, keyToPress: "altKey", });
    };

    // create body element
    windified.$body = windified.$window.div({ class: "body", });
    // apply optional body color
    if (windified.options.bodyColor) windified.$body.css($$.color.contrastStyle(windified.options.bodyColor));

    // make window body content
    if (options.contentMaker) options.contentMaker.call(windified, windified.$body);

    //
    //                              FINISH INITIALIZATION

    // save windify object globally
    windified.global.instances[windified.windowId] = windified;

    // make window the active window
    windified.active();

    // execute any eventually attached global event
    _.each(windified.global.events.create, function (attachedMethObj) { attachedMethObj.method(windified); });

    // return windified object
    return windified;

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ATTACH GLOBAL EVENTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: on global windify events
    ARGUMENTS: (
      !eventName <"create"|"destroy">,
      !methodFunc <function> « function to execute when event is triggered »,
    )
    RETURN: <{
      id: <string> « uuid automatically generated to identify this method »,
      method: <function(...)> « the method you passed as methodFunc argument »,
    }>
  */
  on: function (eventName, methodFunc) {
    var methObj = {
      id: $$.uuid(),
      method: methodFunc,
    };
    windify.global.events[eventName].push(methObj);
    return methObj;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET CURRENTLY ACTIVE WINDOW
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get currently active window
    ARGUMENTS: ( ø )
    RETURN: <windified>
  */
  getActiveWindow: function () {
    return windify.global.selectionHistory[0];
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CALCULATE FULL WINDIFY OPTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: calculate windify options
    ARGUMENTS: ( ?options <windify·options> )
    RETURN: <windify·options>
  */
  calculateOptions: function (options) {

    options = $$.defaults(
      // default options
      { draggable: true, },
      // passed options
      options,
      // windify options maker
      _.isFunction(options.windifyOptionsMaker) ? options.windifyOptionsMaker(options) : undefined
    );

    // make sure container is yquerj element
    options.$container = $(options.$container || "body");

    // return final options
    return options;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SHORTCUTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: shortcuts handling object
    TYPE: <{
      list: <shortcutsListObject>,
      add: <function(<shortcutsListObject>)> « add global shortcuts that can be used in all windows (and will be triggered on the currently active window) »,
    }>
    TYPES:
      shortcutsListObject = <{
        [shortcut string]: {
          !description: <string> « a description of what this shortcut is doing »,
          !action: <function(
            !event,
            ?windified « currently active windify window »,
          ):<void>>
        }>,
      }>;
  */
  shortcuts: "WILL BE AUTOFILLED AFTER WINDIFY OBJECT CREATION",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

windify.shortcuts = require("./shortcuts")(windify);

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = windify;
