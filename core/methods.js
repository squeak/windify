var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE CURRENTLY ACTIVE WINDIFIED
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: make this window the active one
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  active: function () {
    var windified = this;
    windified.global.selectionHistory.unshift(windified);
    $(".windify").removeClass("active");
    windified.$window.addClass("active");
    windified.$window.css("z-index", windified.global.topWindowZIndex);
    windified.global.topWindowZIndex++;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  WINDIFY WINDOW REFRESHING
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: recalculate windify options
    ARGUMENTS: ( ?options <windify·options> )
    RETURN: <windify·options>
  */
  recalculateOptions: function (newOptions) {
    var windified = this;
    if (newOptions) windified.options = $$.defaults(windified.options, newOptions);
    windified.options = windified.calculateOptions(windified.options);
  },

  /**
    DESCRIPTION: recalculate display of titlebar and body color (but does not refresh the window body content)
    ARGUMENTS: ( ?newOptions <windify·options|false> )
    RETURN: <void>
  */
  refreshDisplay: function (newOptions) {
    var windified = this;

    // recalculate windified options
    windified.recalculateOptions(newOptions);

    // remake titlebar
    windified.titlebar.remake(windified.makeTitleBarDisplayOptions());

    // recalculate body color
    if (windified.options.bodyColor) windified.$body.css($$.color.contrastStyle(windified.options.bodyColor));

  },

  /**
    DESCRIPTION: refresh window (titlebar, body color and body content)
    ARGUMENTS: ( ?newOptions <windify·options> )
    RETURN: <void>
  */
  refresh: function (newOptions) {
    var windified = this;

    // recalculate options, refresh display of titlebar and body color
    windified.refreshDisplay();

    // refresh body content
    if (_.isFunction(windified.options.contentMaker)) {
      windified.$body.empty();
      windified.options.contentMaker.call(windified, windified.$body);
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE TITLE BAR OPTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: figure out options to pass to title bar in order to create it's display
    ARGUMENTS: ( ø )
    RETURN: <{
      title: <string>,
      comment: <string>,
      avatar: <string>,
      avatarImage: <string>,
    }>
  */
  makeTitleBarDisplayOptions: function () {
    var windified = this;
    return {
      title: windified.options.title || "",
      comment: windified.options.subtitle || "",
      avatar: windified.options.icon,
      avatarImage: windified.options.image,
    };
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CLOSE WINDOW
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: close this window, executing close option if there is one, else execute destroy method below
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  close: function () {
    var windified = this;
    if (_.isFunction(windified.options.close)) windified.options.close.call(windified)
    else windified.destroy();
  },

  /**
    DESCRIPTION: destroy this window
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  destroy: function () {
    var windified = this;
    windified.$window.remove();
    // remove window to destroy from selection history
    windified.global.selectionHistory = _.reject(windified.global.selectionHistory, function (thisWindified) { return thisWindified.windowId == windified.windowId; })
    delete windified.global.instances[windified.windowId];
    // make sure to make last selected windified active (change it's display)
    if (windified.getActiveWindow()) windified.getActiveWindow().active();
    // execute any eventually attached global event
    _.each(windified.global.events.destroy, function (attachedMethObj) { attachedMethObj.method(windified); });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
