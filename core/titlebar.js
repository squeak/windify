var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/color");
var uify = require("uify");

module.exports = function (windified) {

  var defaultButtons = [
    {
      name: "close",
      title: "close\n(shift + w)",
      icomoon: "cross",
      click: function () {
        windified.close();
      },
      // key: "shift + w", // handled globally
    },
  ];

  // create titlebar item
  var item = uify.item($$.defaults({
    $container: windified.$window,
    class: "titlebar",
    buttons: $$.array.merge(windified.options.buttons, defaultButtons),
    buttonsContext: windified.options.buttonsContext,
  }, windified.makeTitleBarDisplayOptions()));

  // apply optional title bar color
  if (windified.options.titleBarColor) item.$el.css($$.color.contrastStyle(windified.options.titleBarColor));

  // return title bar item
  return item;

};
